(function() {
  /* global angular */
  
  var edugeocachePodatki = function($http) {
    var koordinateTrenutneLokacije = function(lat, lng) {
      return $http.get('/api/lokacije?lng=' + lng + '&lat=' + lat + '&maxRazdalja=100');
    };
    return {
      koordinateTrenutneLokacije: koordinateTrenutneLokacije
    };
  };
  edugeocachePodatki.$inject = ['$http'];
  
  angular
    .module('edugeocache')
    .service('edugeocachePodatki', edugeocachePodatki);
})();