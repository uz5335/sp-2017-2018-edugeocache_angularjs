(function() {
  /* global angular */
  angular.module('edugeocache', ['ngRoute']);
  
  function nastavitev($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'seznam/seznam.pogled.html',
        controller: 'seznamCtrl',
        controllerAs: 'vm'
      })
      .otherwise({redirectTo: '/'});
  }
  
  angular
    .module('edugeocache')
    .config(['$routeProvider', nastavitev]);
})();